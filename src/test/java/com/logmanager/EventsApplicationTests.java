package com.logmanager;

import com.logmanager.events.EventRepo;
import com.logmanager.events.EventService;
import com.logmanager.events.entity.EventFromFile;
import com.logmanager.events.entity.State;
import org.assertj.core.api.Assertions;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Arrays;
import java.util.List;
import java.util.Map;



@RunWith(SpringRunner.class)
public class EventsApplicationTests {

	@Mock
	private EventRepo eventRepo;

	private EventService eventService = new EventService(eventRepo);

	public String EXAMPLE = "{\"id\": \"aabb\", \"state\": \"STARTED\", \"timestamp\": 1636134215552}\n" +
			"{\"id\": \"abab\", \"state\": \"STARTED\", \"timestamp\": 1636134215552}\n" +
			"{\"id\": \"aabb\", \"state\": \"FINISHED\", \"timestamp\": 1636134215554}\n" +
			"{\"id\": \"abab\", \"state\": \"FINISHED\", \"timestamp\": 1636134215559}";


	@Before
	public void setup() {
		MockitoAnnotations.initMocks(this);
	}

	@Test
	public void testEventIds() {
		EventFromFile event1 = new EventFromFile("abc", State.STARTED, "LOG", "12345", 1234567890L);
		EventFromFile event2 = new EventFromFile("xyz", State.STARTED, "LOG", "12345", 1234567890L);
		EventFromFile event3 = new EventFromFile("abc", State.FINISHED, "LOG", "12345", 1234567818L);
		EventFromFile event4 = new EventFromFile("xyz", State.FINISHED, "LOG", "12345", 1234567890L);
		List<EventFromFile> eventFromFiles = Arrays.asList(event1, event2, event3, event4);
		List<String> eventIds = eventService.getEventIds(eventFromFiles);
		Assertions.assertThat(eventIds).hasSize(2);
		Assertions.assertThat(eventIds).contains(event1.getId());
		Assertions.assertThat(eventIds).contains(event2.getId());
	}

	@Test
	public void testParse() {
		EventFromFile event1 = new EventFromFile("aabb", State.STARTED, null, null, 1636134215552L);
		EventFromFile event2 = new EventFromFile("abcd", State.STARTED, null, null, 1636134215552L);
		List<EventFromFile> parsed = eventService.parse(EXAMPLE);
		Assertions.assertThat(parsed).isNotNull();
		Assertions.assertThat(parsed).hasSize(4);
		Assertions.assertThat(parsed).contains(event1);
		Assertions.assertThat(parsed).anyMatch(event -> event.getId().equals("abab"));
		Assertions.assertThat(parsed).doesNotContain(event2);
	}

	@Test
	public void testConvert() {
		EventFromFile event1 = new EventFromFile("aaa", State.STARTED, "LOG", "12345", 1234567222L);
		EventFromFile event2 = new EventFromFile("bbb", State.STARTED, null, null, 1234567222L);
		EventFromFile event3 = new EventFromFile("aaa", State.FINISHED, "LOG", "12345", 1234567224L);
		EventFromFile event4 = new EventFromFile("bbb", State.FINISHED, null, null, 1234567228L);
		List<EventFromFile> eventsFromFiles = Arrays.asList(event1, event2, event3, event4);
		Map<String, EventFromFile> started = eventService.convert(eventsFromFiles, State.STARTED);
		Map<String, EventFromFile> finished = eventService.convert(eventsFromFiles, State.FINISHED);
		Assertions.assertThat(started.values()).contains(event1, event2);
		Assertions.assertThat(finished.values()).contains(event3, event4);
		Assertions.assertThat(started.keySet()).hasSize(2);
	}
}
