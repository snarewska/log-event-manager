package com.logmanager.events;

import org.springframework.web.bind.annotation.*;

@RequestMapping(value = "/log")
@RestController
public class EventController {

    private final EventService eventService;

    public EventController(EventService eventService) {
        this.eventService = eventService;
    }

    @RequestMapping(value = "/process", method = RequestMethod.GET)
    public void processLog(@RequestParam String path) {
        eventService.processLog(path);
    }

    @GetMapping(value = "/process/lines")
    public void processLogLineByLine(@RequestParam String path) {
        eventService.processLogLineByLine(path);
    }

}
