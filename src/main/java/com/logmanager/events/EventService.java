package com.logmanager.events;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.logmanager.events.entity.Event;
import com.logmanager.events.entity.EventFromFile;
import com.logmanager.events.entity.State;
import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Service;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.*;
import java.util.stream.Collectors;

@Log4j2
@Service
public class EventService {

    private static final String SUCCESS_MSG = "Process finished successfully";
    private final EventRepo eventRepo;

    private final ObjectMapper jsonMapper = new ObjectMapper();

    public EventService(EventRepo eventRepo) {
        this.eventRepo = eventRepo;
    }

    public void processLog(String path) {
        String content = readFile(path);
        List<EventFromFile> events = parse(content);
        insert(events);
        log.info(SUCCESS_MSG);
    }

    public void processLogLineByLine(String path) {
        List<EventFromFile> events;
        try {
            events = readFileByLine(path);
        } catch (IOException e) {
            throw new RuntimeException("File cannot be read " + e.getLocalizedMessage());
        }
        insert(events);
        log.info(SUCCESS_MSG);
    }

    private String readFile(String path) {
        try {
            return Files.readString(Path.of(path), StandardCharsets.US_ASCII);
        } catch (Exception e) {
            throw new RuntimeException("File cannot be read " + e.getMessage());
        }
    }

    private List<EventFromFile> readFileByLine(String path) throws IOException {
        try (FileInputStream fis = new FileInputStream(new File(path));
             BufferedReader br = new BufferedReader(new InputStreamReader(fis))) {
            List<EventFromFile> events = new ArrayList<>();
            String line;
            while ((line = br.readLine()) != null) {
                EventFromFile event = jsonMapper.readValue(line, EventFromFile.class);
                events.add(event);
            }
            return events;
        }
    }

    public List<EventFromFile> parse(String text) {
        String replaced = text.replace("}", "},");
        String substring = replaced.substring(0, replaced.length() - 1);
        String jsonArrayAsString = new StringBuilder(substring).insert(0, "[")
                .insert(substring.length() + 1, "]").toString();
        try {
            return jsonMapper.readValue(jsonArrayAsString, new TypeReference<List<EventFromFile>>() {});
        } catch (Exception e) {
            throw new RuntimeException("Error in parsing" + e.getMessage());
        }
    }
    public List<String> getEventIds(List<EventFromFile> events) {
        return events.stream()
                .map(event -> event.getId())
                .distinct()
                .collect(Collectors.toList());
    }

    public Map<String, EventFromFile> convert(List<EventFromFile> events, State state) {
        Map<String, EventFromFile> map = new HashMap<>();
        for (EventFromFile event : events) {
            if (event.getState().equals(state)) {
                map.put(event.getId(), event);
            }
        }
        return map;
    }

    private void insert(List<EventFromFile> events) {
        Collections.sort(events);
        Map<String, EventFromFile> started = convert(events, State.STARTED);
        Map<String, EventFromFile> finished = convert(events, State.FINISHED);
        List<String> eventIds = getEventIds(events);
        for (String id : eventIds) {
            EventFromFile finishedEvent = finished.get(id);
            long duration = finishedEvent.getTimestamp() - started.get(id).getTimestamp();
            Event event = new Event(id, duration, finishedEvent.getType(), finishedEvent.getHost(), checkDuration(duration));
            eventRepo.save(event);
        }
    }

    private boolean checkDuration(Long duration) {
        return duration > 4;
    }
}
