package com.logmanager.events.entity;

public enum State {
    STARTED, FINISHED;
}
