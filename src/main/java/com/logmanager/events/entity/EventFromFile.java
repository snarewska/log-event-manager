package com.logmanager.events.entity;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;

import java.util.Objects;

@Getter
public class EventFromFile implements Comparable<EventFromFile> {

    private final String id;

    private final State state;

    private final String type;

    private final String host;

    private final Long timestamp;

    @JsonCreator
    public EventFromFile(@JsonProperty("id") String id,
                         @JsonProperty("state") State state,
                         @JsonProperty("type") String type,
                         @JsonProperty("host") String host,
                         @JsonProperty("timestamp") Long timestamp) {
        this.id = id;
        this.state = state;
        this.type = type;
        this.host = host;
        this.timestamp = timestamp;
    }

    @Override
    public int compareTo(EventFromFile o) {
        return this.getId().compareTo(o.getId());
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        EventFromFile that = (EventFromFile) o;
        return this.id.equals(that.id) && this.state == that.state;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, state);
    }
}
