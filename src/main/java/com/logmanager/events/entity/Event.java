package com.logmanager.events.entity;

import lombok.Getter;

import javax.persistence.*;

@Getter
@Entity
@Table(name = "events")
public class Event {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private String eventId;

    private  Long duration;

    private String type;

    private String host;

    private boolean alert;

    public Event(String eventId, Long duration, String type, String host, boolean alert) {
        this.eventId = eventId;
        this.duration = duration;
        this.type = type;
        this.host = host;
        this.alert = alert;
    }

    public Event() {

    }

    @Override
    public String toString() {
        return "Event{" +
                "id='" + eventId + '\'' +
                ", duration=" + duration +
                ", type='" + type + '\'' +
                ", host='" + host + '\'' +
                ", alert=" + alert +
                '}';
    }
}
